package handlers

import (
	"context"
	"widi_request_sim_modules/entities"
	"widi_request_sim_modules/repositories"
	"widi_request_sim_modules/services"

	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
)

type ServiceModules struct {
	context context.Context
	repo    repositories.Repository
	db      *gorm.DB
}

func NewServiceModules(db *gorm.DB) *ServiceModules {
	repo := services.NewService(db)

	var ctx context.Context
	return &ServiceModules{
		context: ctx,
		repo:    repo,
		db:      db,
	}

}

func (s ServiceModules) AddRequest(ctx context.Context, req *entities.Requester) error {
	err := s.repo.AddRequest(ctx, *req)
	if err != nil {
		logrus.Error(err.Error())
		return err
	}

	return nil
}

func (s ServiceModules) EditRequest(ctx context.Context, req *entities.Requester) error {
	err := s.repo.EditRequest(ctx, *req)
	if err != nil {
		logrus.Error(err.Error())
		return err
	}
	return nil
}

func (s ServiceModules) DeleteRequest(ctx context.Context, req *entities.Requester) error {
	err := s.repo.DeleteRequest(ctx, *req)
	if err != nil {
		logrus.Error(err.Error())
		return err
	}
	return nil
}
