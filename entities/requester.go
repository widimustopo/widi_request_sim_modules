package entities

import (
	"github.com/gofrs/uuid"
)

type Requester struct {
	IdRequest     uuid.UUID `json:"id_request" gorm:"primary_key; unique;type:uuid; column:id_request;default:uuid_generate_v4()"`
	NoKtp         string    `json:"no_ktp" validate:"required"`
	FullName      string    `json:"full_name" validate:"required"`
	Address       string    `json:"address" validate:"required"`
	Gender        string    `json:"gender" validate:"required"`
	Religion      string    `json:"religion" validate:"required"`
	Work          string    `json:"work" validate:"required"`
	Status        string    `json:"status" validate:"required"`
	StatusRequest int8      `json:"status_request" validate:"required"`
}
