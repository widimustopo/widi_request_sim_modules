module widi_request_sim_modules

go 1.15

require (
	github.com/fsnotify/fsnotify v1.4.9
	github.com/gofrs/uuid v4.0.0+incompatible
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/viper v1.8.1
	github.com/streadway/amqp v1.0.0
	gorm.io/driver/mysql v1.1.1
	gorm.io/gorm v1.21.9
)
