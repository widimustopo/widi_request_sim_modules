package libs

import (
	"github.com/fsnotify/fsnotify"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

type Config struct {
	ServerPort       string
	DatabasePort     string
	DatabaseUser     string
	DatabasePassword string
	DatabaseHost     string
	DatabaseName     string
	Amqp             AMQP
}

type AMQP struct {
	//add
	RabbitMQURL                    string
	RabbitMQRequestQueue           string
	RabbitMQExchangeRequestDeclare string
	RabbitMQExchangeRequest        string
	RabbitMqRequestBind            string
	//edit
	RabbitMQExchangeEditRequest        string
	RabbitMQExchangeEditRequestDeclare string
	RabbitMQEditRequestQueue           string
	RabbitMqEditRequestBind            string
	//delete
	RabbitMQExchangeDeleteRequest        string
	RabbitMQExchangeDeleteRequestDeclare string
	RabbitMQDeleteRequestQueue           string
	RabbitMqDeleteRequestBind            string
}

var config Config

//GetConfig : Get Current Config
func GetConfig() Config {
	return config
}

//SetConfig set configuraton
func SetConfig(cfg Config) {
	config = cfg
}

//OnConfigurationChange : Set on configuration file change
func (config *Config) OnConfigurationChange(onConfigChange func(e fsnotify.Event)) {
	viper.OnConfigChange(onConfigChange)
}

func LoadConfig() *Config {
	viper.SetConfigType("yml")
	viper.AddConfigPath(".")
	viper.SetConfigName("config.yml")

	var cfg Config
	if err := viper.ReadInConfig(); err != nil {
		logrus.Errorf("Error reading config file %v", err)
	}

	err := viper.Unmarshal(&cfg)
	if err != nil {
		logrus.Errorf("Unable to decode into struct, %v", err)
	}
	viper.WatchConfig()

	cfg.Amqp = AMQP{
		RabbitMQURL: viper.GetString("RabbitMQURL"),
		//add
		RabbitMQRequestQueue:           viper.GetString("RabbitMQRequestQueue"),
		RabbitMQExchangeRequestDeclare: viper.GetString("RabbitMQExchangeRequestDeclare"),
		RabbitMQExchangeRequest:        viper.GetString("RabbitMQExchangeRequest"),
		RabbitMqRequestBind:            viper.GetString("RabbitMqRequestBind"),
		//edit
		RabbitMQExchangeEditRequest:        viper.GetString("RabbitMQExchangeEditRequest"),
		RabbitMQExchangeEditRequestDeclare: viper.GetString("RabbitMQExchangeEditRequestDeclare"),
		RabbitMQEditRequestQueue:           viper.GetString("RabbitMQEditRequestQueue"),
		RabbitMqEditRequestBind:            viper.GetString("RabbitMqEditRequestBind"),
		//delete
		RabbitMQExchangeDeleteRequest:        viper.GetString("RabbitMQExchangeDeleteRequest"),
		RabbitMQExchangeDeleteRequestDeclare: viper.GetString("RabbitMQExchangeDeleteRequestDeclare"),
		RabbitMQDeleteRequestQueue:           viper.GetString("RabbitMQDeleteRequestQueue"),
		RabbitMqDeleteRequestBind:            viper.GetString("RabbitMqDeleteRequestBind"),
	}

	SetConfig(cfg)
	return &cfg
}
