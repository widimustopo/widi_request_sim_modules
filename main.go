package main

import (
	"widi_request_sim_modules/database"
	"widi_request_sim_modules/libs"
	"widi_request_sim_modules/listener"
)

func main() {
	//load config from config.yml
	loadConfig := libs.LoadConfig()

	//initiate database connection
	db := database.OpenDB(loadConfig)

	//run consumer
	forever := make(chan bool)
	defer close(forever)
	//load config rabbitmq
	rmq := listener.Init(&loadConfig.Amqp)

	rmq.DeclareConsumer()
	go rmq.ConsumerRequest(db)

	rmq.DeclareEditConsumer()
	go rmq.ConsumerEditRequest(db)

	rmq.DeclareDeleteConsumer()
	go rmq.ConsumerDeleteRequest(db)

	<-forever

}
