package repositories

import (
	"context"
	"widi_request_sim_modules/entities"
)

type Repository interface {
	AddRequest(ctx context.Context, req entities.Requester) error
	EditRequest(ctx context.Context, req entities.Requester) error
	DeleteRequest(ctx context.Context, req entities.Requester) error
}
