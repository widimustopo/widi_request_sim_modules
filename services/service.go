package services

import (
	"context"
	"fmt"
	"widi_request_sim_modules/entities"
	"widi_request_sim_modules/repositories"

	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
)

type Service struct {
	*gorm.DB
}

func NewService(db *gorm.DB) repositories.Repository {
	return Service{db}
}

func (s Service) AddRequest(ctx context.Context, req entities.Requester) error {
	req.StatusRequest = 0
	err := s.DB.Create(req).Error
	if err != nil {
		logrus.Error(err.Error())
		return err
	}
	fmt.Println("consume add : ", req)
	return nil
}

func (s Service) EditRequest(ctx context.Context, req entities.Requester) error {

	result, err := s.FindByID(req.IdRequest.String())
	if err != nil || result == nil {
		logrus.Error(err.Error())
		return err
	}

	var requester entities.Requester

	requester.IdRequest = req.IdRequest

	errUpdate := s.DB.Model(&requester).UpdateColumns(entities.Requester{
		NoKtp:         req.NoKtp,
		FullName:      req.FullName,
		Address:       req.Address,
		Gender:        req.Gender,
		Religion:      req.Religion,
		Work:          req.Work,
		Status:        req.Status,
		StatusRequest: req.StatusRequest,
	}).Error
	if errUpdate != nil {
		logrus.Error(errUpdate.Error())
		return errUpdate
	}
	fmt.Println("consume edit : ", req)
	return nil
}

func (s Service) DeleteRequest(ctx context.Context, req entities.Requester) error {
	result, err := s.FindByID(req.IdRequest.String())
	if err != nil || result == nil {
		logrus.Error(err.Error())
		return err
	}
	var requester entities.Requester

	requester.IdRequest = req.IdRequest

	errDelete := s.DB.Model(&requester).Delete(&requester, "id_request = ?", req.IdRequest).Error

	if errDelete != nil {
		logrus.Error(err.Error())
		return err
	}

	fmt.Println("consume delete : ", req.IdRequest)
	return nil
}

func (p Service) FindByID(idRequest string) (*entities.Requester, error) {
	var requester entities.Requester

	err := p.DB.First(&requester, "id_request = ?", idRequest).Error
	if err != nil {
		logrus.Info("err ", requester)
		return nil, err
	}
	return &requester, nil
}
