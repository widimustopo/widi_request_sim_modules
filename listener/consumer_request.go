package listener

import (
	"context"
	"encoding/json"
	"time"
	"widi_request_sim_modules/entities"
	"widi_request_sim_modules/handlers"

	log "github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
	"gorm.io/gorm"
)

//DeclareConsumer : for declaring a consumer request in the service
func (rmq *RabbitMQ) DeclareConsumer() {
	var err error
	queueDeclare, err := rmq.Chann.QueueDeclare(rmq.RabbitMQRequestQueue, true, false, false, false, nil)
	if err != nil {
		log.Debug(err.Error())
	}

	err = rmq.Chann.QueueBind(queueDeclare.Name, rmq.RabbitMqRequestBind, rmq.RabbitMQExchangeRequest, false, nil)
	if err != nil {
		log.Debug(err.Error())
	}
	err = rmq.Chann.Qos(1, 0, false)
	if err != nil {
		log.Debug(err.Error())
	}
}

//ConsumerRequest : consume request data
func (rmq *RabbitMQ) ConsumerRequest(db *gorm.DB) {
	published, err := rmq.Chann.Consume(
		rmq.RabbitMQRequestQueue,
		"request-consumer",
		false,
		false,
		false,
		false,
		nil)

	if err != nil {
		log.Info(err.Error())
	}

	log.Info("Running Consumer Request SIM RabbitMQ")
	for {
		select {
		case d, ok := <-published:
			if !ok {
				return
			}
			dataConsume := entities.Requester{}
			json.Unmarshal(d.Body, &dataConsume)
			newHandler := handlers.NewServiceModules(db)
			ctx := context.Background()
			err := newHandler.AddRequest(ctx, &dataConsume)

			if err != nil {
				log.Error(err.Error())
			} else {
				rmq.Chann.QueueDelete(rmq.RabbitMQRequestQueue, false, false, true)
			}

		}
	}
}

func PublishAddResponse(req entities.Requester) {
	rmq := RabbitMQClient
	body, _ := json.Marshal(req)

	err := rmq.Chann.Publish(rmq.RabbitMQExchangeRequest, rmq.RabbitMqRequestBind, false, false, amqp.Publishing{
		DeliveryMode: amqp.Persistent,
		Timestamp:    time.Now(),
		ContentType:  "application/json",
		Body:         body,
	})
	if err != nil {
		log.Error(err.Error())
	}

	log.Info("Succes publish data Responses : ", req)
}
