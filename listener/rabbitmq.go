package listener

import (
	"time"
	"widi_request_sim_modules/libs"

	log "github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
)

type RabbitMQ struct {
	URL                            string
	RabbitMQRequestQueue           string
	RabbitMQExchangeRequestDeclare string
	RabbitMQExchangeRequest        string
	RabbitMqRequestBind            string
	//edit
	RabbitMQExchangeEditRequest        string
	RabbitMQExchangeEditRequestDeclare string
	RabbitMQEditRequestQueue           string
	RabbitMqEditRequestBind            string
	//get
	RabbitMQExchangeGetRequest        string
	RabbitMQExchangeGetRequestDeclare string
	RabbitMQGetRequestQueue           string
	RabbitMqGetRequestBind            string
	//delete
	RabbitMQExchangeDeleteRequest        string
	RabbitMQExchangeDeleteRequestDeclare string
	RabbitMQDeleteRequestQueue           string
	RabbitMqDeleteRequestBind            string
	//Connections
	Conn       *amqp.Connection
	Chann      *amqp.Channel
	closeChann chan *amqp.Error
	quitChann  chan bool
}

var RabbitMQClient RabbitMQ

func Init(config *libs.AMQP) *RabbitMQ {

	rmq := &RabbitMQ{
		URL:                            config.RabbitMQURL,
		RabbitMQRequestQueue:           config.RabbitMQRequestQueue,
		RabbitMQExchangeRequestDeclare: config.RabbitMQExchangeRequestDeclare,
		RabbitMQExchangeRequest:        config.RabbitMQExchangeRequest,
		RabbitMqRequestBind:            config.RabbitMqRequestBind,
		//edit
		RabbitMQExchangeEditRequest:        config.RabbitMQExchangeEditRequest,
		RabbitMQExchangeEditRequestDeclare: config.RabbitMQExchangeEditRequestDeclare,
		RabbitMQEditRequestQueue:           config.RabbitMQEditRequestQueue,
		RabbitMqEditRequestBind:            config.RabbitMqEditRequestBind,
		//delete
		RabbitMQExchangeDeleteRequest:        config.RabbitMQExchangeDeleteRequest,
		RabbitMQExchangeDeleteRequestDeclare: config.RabbitMQExchangeDeleteRequestDeclare,
		RabbitMQDeleteRequestQueue:           config.RabbitMQDeleteRequestQueue,
		RabbitMqDeleteRequestBind:            config.RabbitMqDeleteRequestBind,
	}

	err := rmq.startConnection()
	if err != nil {
		log.Fatalf("Connection RabbitMQ: %v", err)
	}

	rmq.quitChann = make(chan bool)

	RabbitMQClient = *rmq

	go rmq.handleDisconnect()
	return rmq
}

func (rmq *RabbitMQ) startConnection() error {
	var err error
	rmq.Conn, err = amqp.Dial(rmq.URL)
	if err != nil {
		return err
	}
	rmq.Chann, err = rmq.Conn.Channel()
	if err != nil {
		return err
	}
	return nil
}

// handle a disconnection trying to reconnect every 5 seconds
func (rmq *RabbitMQ) handleDisconnect() {
	for {
		select {
		case errChann := <-rmq.closeChann:
			if errChann != nil {
				log.Errorf("rabbitMQ disconnection: %v", errChann)
			}
		case <-rmq.quitChann:
			rmq.Conn.Close()
			log.Info("rabbitMQ has been shut down")
			rmq.quitChann <- true
			return
		}
		log.Info("trying to reconnect to rabbitMQ...")
		time.Sleep(5 * time.Second)
		if err := rmq.startConnection(); err != nil {
			log.Errorf("rabbitMQ error: %v", err)
		}
	}
}
